<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Piksel_Starter
 */

get_header(); ?>
    <div class="container single-blog-post-container">
        <?php if($isMobile->isMobile() || $isMobile->isTablet()): ?>

            <!-- Mobile Ad -->
            <div class="advert-container advert-singe">
                <span><?php _e('advertisment', 'pstarster'); ?></span>

                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- baner1 -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-8248104886241417"
                     data-ad-slot="3615716289"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            <!-- Mobile Ad End-->
        <?php else: ?>
            <!-- Desktop Ad -->
            <div class="col-lg-12">
                <div class="advert-container advert-singe">
                    <span><?php _e('advertisment', 'pstarster'); ?></span>

                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Top horizontal -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-8248104886241417"
                         data-ad-slot="9262538584"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
            <!-- Desktop Ad End-->
        <?php endif; ?>
        <?php
        while ( have_posts() ) : the_post();

            get_template_part( 'template-parts/content', 'single');

        endwhile;  wp_reset_postdata(); // End of the loop.
        ?>

        <div class="col-sidebar">
            <?php dynamic_sidebar('sidebar-1'); ?>
        </div>

    </div>

<?php

//get_sidebar();
get_footer();
