jQuery(document).ready(function ($) {
    var images = $('.col-recipe-content img');
    $.each(images, function (key, value) {
        if($(value).hasClass('img-responsive') === false) {
            $(value).addClass('img-responsive');
        }
    });


    $("body").tooltip({ selector: '[data-toggle=tooltip]' });

});
