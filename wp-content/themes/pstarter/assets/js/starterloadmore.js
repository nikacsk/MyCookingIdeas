$(document).ready(function ($) {
    $('#grid_offset').val(7);
});

jQuery(function($){
    $('#home-grid-load-more').click(function(e){
        e.preventDefault();
        var action = 'grid_load_more';
        var grid_offset = $('#grid_offset').val();
        var button = $(this);
        var no_posts_message = $('#home-grid-no-posts');
        var ajaxContainer = $('#ajax_results');
        var data = {
            action: action,
            grid_offset: grid_offset
        };

        $.ajax({
            url : pstarter.ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {
                button.text('Loading...');
            },
            success : function( data ){

                if(data == 0){
                    button.remove();
                    no_posts_message.text(pstarter.no_grid_posts);
                } else {
                    button.text(pstarter.load_button_text);
                    ajaxContainer.append(data);
                    var new_offset = parseInt(grid_offset) + 7;
                    $('#grid_offset').val(new_offset);

                }

            }
        });
    });
});
