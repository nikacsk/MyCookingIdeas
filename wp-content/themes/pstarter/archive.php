<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Piksel_Starter
 */

get_header(); ?>
    <div class="container single-blog-post-container archive-page">
        <div class="col-recipe-content">
            <?php
            the_archive_title( '<h1 class="single-post-blog-title">', '</h1>' );
            the_archive_description( '<div class="archive-description">', '</div>' );
            ?>
            <?php
            if ( have_posts() ) :

                if ( is_home() && ! is_front_page() ) : ?>
                    <header>
                        <h1 class="single-post-blog-title"><?php single_post_title(); ?></h1>
                    </header>

                <?php
                endif;

                /* Start the Loop */
                while ( have_posts() ) : the_post(); ?>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-xxs-12">
                        <div class="grid-post-wrapper post-id-<?php echo get_the_ID(); ?>">
                            <a href="<?php echo get_the_permalink(); ?>">

                                <div class="grid-image-wrap">
                                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'grid-image'); ?>" alt="<?php echo get_the_title(get_the_ID()); ?>" title="<?php echo get_the_title(get_the_ID()); ?>">
                                </div>

                                <div class="post-details">
                                    <div class="grid-title">
                                        <?php echo mb_strimwidth(get_the_title(), 0, 75, '...'); ?>
                                    </div>

                                </div>

                            </a>

                            <ul class="meal-details">
                                <?php if(get_field('_recipe_ready_in', get_the_ID())): ?>
                                    <li>
                                        <i class="fas fa-stopwatch"></i>
                                        <span><?php echo get_field('_recipe_ready_in', get_the_ID()) ?> <?php _e('min'); ?></span>
                                    </li>
                                <?php endif; ?>

                                <?php if(get_field('_recipe_servings', get_the_ID())): ?>
                                    <li>
                                        <i class="fas fa-utensils"></i>
                                        <span><?php echo get_field('_recipe_servings', get_the_ID()); ?> <?php _e('people'); ?></span>
                                    </li>
                                <?php endif; ?>

                                <?php if($skillLevel): ?>
                                    <li>
                                        <i class="fas fa-trophy"></i>
                                        <span><?php echo $skillLevel; ?></span>
                                    </li>
                                <?php endif; ?>


                            </ul>

                        </div>
                    </div>

                <?php

                endwhile;


            else :

                get_template_part( 'template-parts/content', 'none' );

            endif; ?>

            <div class="clearfix"></div>
            <div class="mci-archive-pagination">
                <?php wpbeginner_numeric_posts_nav(); ?>
            </div>
        </div>

        <div class="col-sidebar">
            <?php dynamic_sidebar('sidebar-1'); ?>
        </div>

    </div>
<?php
get_footer();
