<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Piksel_Starter
 */

?>

	</div><!-- #content -->

	<footer id="site_footer" class="site-foote container">
        <div class="col-lg-12 footer-widgets">
            <div class="col-sm-4 col-xs-12">  <?php dynamic_sidebar('footer-1') ?> </div>
            <div class="col-sm-4 col-xs-12">
                <?php dynamic_sidebar('footer-2') ?>
                <div class="clearfix"></div>
                <div class="social-media social-media-footer">

                    <?php _e('Follow us on: ') ?> </br>

                    <a href="https://www.facebook.com/mycookingideasofficial" target="_blank">
                        <i class="fab fa-facebook-square"></i>
                    </a>

                    <a href="https://www.pinterest.com/mycookingideas/" target="_blank">
                        <i class="fab fa-pinterest-square"></i>
                    </a>

                    <a href="https://www.youtube.com/channel/UCq9khpxwZn7OYbSKREaTalQ" target="_blank">
                        <i class="fab fa-youtube-square"></i>
                    </a>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">  <?php dynamic_sidebar('footer-3') ?> </div>
        </div>

        <div class="celarfix"></div>

		<div class="site-info col-xs-12">
            © My Cooking Ideas. All Rights Reserved. |  Developed by <a href="http://labscreative.com" target="_blank">LabsCreative.com</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<div id="rotate"></div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b32bfb1ff110fd1"></script>
</body>
</html>
