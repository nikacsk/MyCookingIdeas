<?php
/*
    Template Name: HOME PAGE
 */

get_header();
?>

<section id="last_two">
    <div class="container no-padding width-100">
        <?php get_template_part('template-parts/home/home', 'last-two'); ?>
    </div>
</section>

<section id="contributors">
    <div class="container width-100">
        <div class="col-sm-8 col-xs-12">
            <h2><?php _e('Interested  in <span class="orange-text">contributing</span> to our open cooking book ?', 'pstarter');?></h2>
            <p><?php _e('Feel free to get in touch with us for more information about contributing', 'pstarter'); ?></p>
        </div>

        <div class="col-sm-4 col-xs-12">
            <a href="https://www.facebook.com/mycookingideasofficial/" target="_blank"><i class="far fa-comments"></i> <?php _e('CONTACT US', 'pstarter') ?></a>
        </div>
    </div>
</section>

<section id="post-grid">
    <div class="container">
        <?php get_template_part('template-parts/home/home', 'grid'); ?>
        <!-- LOAD MORE BUTTON -->

        <span id="ajax_results"></span>
        <div class="clearfix"></div>
        <div id="home-grid-load-more" class="grid-load-more"><?php _e('More recipes', 'pstarter'); ?></div>
        <input id="grid_offset" type="hidden" value="7">
        <div id="home-grid-no-posts"></div>
    </div>
</section>


<section id="home-blog">
    <div class="col-sm-4">
        <?php dynamic_sidebar('middle-1'); ?>
    </div>
    <div class="col-sm-4">
        <?php dynamic_sidebar('middle-2'); ?>
    </div>

    <div class="col-sm-4">
        <?php dynamic_sidebar('middle-3'); ?>
    </div>
</section>

<?php
get_footer();


