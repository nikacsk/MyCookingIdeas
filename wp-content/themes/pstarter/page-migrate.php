<?php
/*
    Template Name: MIGRATE
 */

//$json = file_get_contents('http://oldcooking.local/wp-json/recipes/v1/all');
//$recipes = json_decode($json);
//var_dump($recipes[0]->_embedded); die;
//echo '<pre>';
//print_r($recipes); die;

//$handle = curl_init();

//$url = "http://oldcooking.local/wp-json/recipes/v1/all";
//$url = "http://oldcooking.local/wp-json/posts/v1/all";

// Set the url
//curl_setopt($handle, CURLOPT_URL, $url);
// Set the result output to be a string.
//curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

//$output = curl_exec($handle);

//curl_close($handle);

//$array = json_decode($output);

// var_dump($array); die;

//function Generate_Featured_Image( $image_url, $post_id  ){
//    $upload_dir = wp_upload_dir();
//    $image_data = file_get_contents($image_url);
//    $filename = basename($image_url);
//    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
//    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
//    file_put_contents($file, $image_data);
//
//    $wp_filetype = wp_check_filetype($filename, null );
//    $attachment = array(
//        'post_mime_type' => $wp_filetype['type'],
//        'post_title' => sanitize_file_name($filename),
//        'post_content' => '',
//        'post_status' => 'inherit'
//    );
//    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
//    require_once(ABSPATH . 'wp-admin/includes/image.php');
//    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
//    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
//    $res2= set_post_thumbnail( $post_id, $attach_id );
//}

//$oldDB = new wpdb('root','','cooking_old','localhost');
//
//$oldPosts = $oldDB->get_results("SELECT * FROM ci_posts WHERE post_type = 'recipe' LIMIT 1");
//var_dump($oldPosts); die;
//foreach ($array as $post) {
//
//    $args = [
//
//      'post_date' => $post->post_date,
//      'post_date_gmt' => $post->post_date_gmt,
//      'post_content' => $post->post_content,
//      'post_title' => $post->post_title,
//      'post_excerpt' => $post->post_excerpt,
//      'post_status' => $post->post_status,
////      'post_type' => 'recipe',
//      'post_type' => 'post',
//      'post_name' => $post->post_name,
//      'post_parent' => $post->post_parent,
//
//    ];
//
//    $newPost = wp_insert_post($args);


//    Generate_Featured_Image( $post->featured_image,   $newPost);
//    wp_set_object_terms($newPost, $post->recipe_type, 'recipe_type');
//    wp_set_object_terms($newPost, $post->skill_level, 'skill_level');
//    wp_set_object_terms($newPost, $post->category[0]->name, 'category');

    //yield
//    update_field('field_5b1870ee11629', $post->yield,  $newPost);
//    update_field('field_5b1871231162a', $post->prep_time,  $newPost);
//    update_field('field_5b1871401162b', $post->cook_time,  $newPost);
//    update_field('field_5b1871621162c', $post->ready_in ,  $newPost);
//    update_field('field_5b187211db8f6', $post->video  ,  $newPost);

    //steps

//    $items = $post->steps;
//
//    foreach ($items as $items_value) {
//        $row = [
//            '_preparation_step' => $items_value
//        ];
//
//        add_row('_preparation_steps', $row, $newPost);
//
//    }

    //ingrediants

//    $ingredients = $post->ingredients;
//
//    foreach ($ingredients as $items_value) {
//        $row = [
//            '_recipe_ingredient' => $items_value
//        ];
//
//        add_row('_recipe_ingredients', $row, $newPost);
//
//    }



//}

/* MAKING META DATA FOR QUERYING BY INGREDIENT */

$reciepes = new WP_Query([
    'post_type' => 'recipe',
    'post_status' => 'publish',
    'posts_per_page' => -1
]);

while ($reciepes->have_posts()) {
    $reciepes->the_post();
    $id = get_the_ID();

    echo get_the_title();
    $meta_key = '_ingredients_for_querying';
    delete_post_meta($id, $meta_key);
    $saved_values = array();

    if(have_rows('_recipe_ingredients',$id)) : while(have_rows('_recipe_ingredients',$id)) : the_row();
        $ingredient = get_sub_field('_recipe_ingredient');
        $saved_values[] = $ingredient;
    endwhile; endif;

    add_post_meta($id, $meta_key, serialize($saved_values), true);
    $saved_values = [];
}

echo 'script done';

//$string = file_get_contents(get_template_directory().'/assets/old.json');
//$json_a = json_decode($string, true);
//var_dump($json_a);

