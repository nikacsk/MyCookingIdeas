<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Piksel_Starter
 */

get_header(); ?>
    <div class="container single-blog-post-container">
        <?php
        while ( have_posts() ) : the_post();

            get_template_part( 'template-parts/content', 'page');


        endwhile; // End of the loop.
        ?>

        <div class="col-sidebar">
            <?php dynamic_sidebar('sidebar-1'); ?>
        </div>

    </div>

<?php

//get_sidebar();
get_footer();
