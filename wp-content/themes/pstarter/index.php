<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Piksel_Starter
 */

get_header(); ?>

    <div class="container single-blog-post-container">
    <div class="col-recipe-content">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="single-post-blog-title"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) : the_post();

                $terms = get_the_terms(get_the_ID(), 'recipe_type');
                $terms2 = get_the_terms(get_the_ID(), 'skill_level');
                $skillLevel ='';
//            var_dump($terms2);

                if($terms2 != false){
                    foreach ($terms2 as $term) {
                        $skillLevel .= $term->name. ' ';
                    }
                }


                ?>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-xxs-12">
                    <div class="grid-post-wrapper post-id-<?php echo get_the_ID(); ?>">
                        <a href="<?php echo get_the_permalink(); ?>">

                            <div class="grid-image-wrap">
                                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'grid-image'); ?>" alt="<?php echo get_the_title(get_the_ID()); ?>" title="<?php echo get_the_title(get_the_ID()); ?>">
                            </div>

                            <div class="post-details">
                                <div class="grid-title">
                                    <?php echo mb_strimwidth(get_the_title(), 0, 75, '...'); ?>
                                </div>

                            </div>

                        </a>
                        <span class="rec-category">
                        <?php
                        if($terms != false) {
                            foreach ($terms as $term) {
                                $termLink = get_term_link($term);
                                echo "<a href='{$termLink}'>{$term->name}</a>";
                            }
                        }
                        ?>
                     </span>

                        <ul class="meal-details">
                            <?php if(get_field('_recipe_ready_in', get_the_ID())): ?>
                                <li>
                                    <i class="fas fa-stopwatch"></i>
                                    <span><?php echo get_field('_recipe_ready_in', get_the_ID()) ?> <?php _e('min'); ?></span>
                                </li>
                            <?php endif; ?>

                            <?php if(get_field('_recipe_servings', get_the_ID())): ?>
                                <li>
                                    <i class="fas fa-utensils"></i>
                                    <span><?php echo get_field('_recipe_servings', get_the_ID()); ?> <?php _e('people'); ?></span>
                                </li>
                            <?php endif; ?>

                            <?php if($skillLevel): ?>
                                <li>
                                    <i class="fas fa-trophy"></i>
                                    <span><?php echo $skillLevel; ?></span>
                                </li>
                            <?php endif; ?>


                        </ul>

                    </div>
                </div>

            <?php

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

         </div>

        <div class="col-sidebar">
            <?php dynamic_sidebar('sidebar-1'); ?>
        </div>

    </div>

<?php
get_footer();
