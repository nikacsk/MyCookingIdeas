<?php
$args = array(
    'posts_per_page' => 7,
    'post_type' => 'recipe',
    'orderby' => 'date',
    'order' => 'DESC',
    'post_status' => 'publish',
    'offset' => 2
);


$grid = new WP_Query($args);
$count =0;
if($grid->have_posts()) : while ($grid->have_posts()): $count++; $grid->the_post();

    $terms = get_the_terms(get_the_ID(), 'recipe_type');
    $terms2 = get_the_terms(get_the_ID(), 'skill_level');
    $skillLevel ='';
            if($terms2 != false) {
                foreach ($terms2 as $term) {
                    $skillLevel .= $term->name . ' ';
                }
            }
    ?>

    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
        <div class="grid-post-wrapper post-id-<?php echo get_the_ID(); ?>">
            <a href="<?php echo get_the_permalink(); ?>">
<!--                --><?php //the_post_thumbnail('grid-image', array('class'=>'img-responsive')); ?>

                <div class="grid-image-wrap">
                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'grid-image'); ?>" alt="<?php echo get_the_title(get_the_ID()); ?>" title="<?php echo get_the_title(get_the_ID()); ?>">
                </div>

                <div class="post-details">
<!--                    <div class="grid-ratings">-->
<!--                        <i class="fas fa-star"></i>-->
<!--                        <i class="fas fa-star"></i>-->
<!--                        <i class="fas fa-star"></i>-->
<!--                        <i class="fas fa-star"></i>-->
<!--                        <i class="far fa-star"></i>-->
<!---->
<!--                        <span class="vote-count">(57)</span>-->
<!--                    </div>-->

                    <div class="grid-title">
                        <?php echo mb_strimwidth(get_the_title(), 0, 75, '...'); ?>
                    </div>

                </div>

            </a>
            <span class="rec-category">
                        <?php
                        if($terms != false) {
                            foreach ($terms as $term) {
                                $termLink = get_term_link($term);
                                echo "<a href='{$termLink}'>{$term->name}</a>";
                            }
                        }
                        ?>
            </span>

            <ul class="meal-details">
                <?php if(get_field('_recipe_ready_in', get_the_ID())): ?>
                    <li>
                        <i class="fas fa-stopwatch"></i>
                        <span><?php echo get_field('_recipe_ready_in', get_the_ID()) ?> <?php _e('min'); ?></span>
                    </li>
                <?php endif; ?>

                <?php if(get_field('_recipe_servings', get_the_ID())): ?>
                    <li>
                        <i class="fas fa-utensils"></i>
                        <span><?php echo get_field('_recipe_servings', get_the_ID()); ?></span>
                    </li>
                <?php endif; ?>

                <?php if($skillLevel): ?>
                    <li>
                        <i class="fas fa-trophy"></i>
                        <span><?php echo $skillLevel; ?></span>
                    </li>
                <?php endif; ?>


            </ul>

        </div>
    </div>

    <?php if($count == 2) : ?>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
            <div class="post-wrapper advert-grid post-id-<?php echo get_the_ID(); ?>">
                <div class="advert-container">
                    <span><?php _e('advertisment', 'pstarster'); ?></span>

                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- baner1.1 -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:278px;height:340px"
                         data-ad-client="ca-pub-8248104886241417"
                         data-ad-slot="1686167885"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php endwhile; wp_reset_postdata(); ?>


<?php else: ?>

    <div class="col-lg-12">
        <h3><?php _e('No posts at the moment', 'pstarter'); ?></h3>
    </div>

<?php endif; ?>
