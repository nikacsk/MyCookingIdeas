<?php

$args = array(
    'posts_per_page' => 2,
    'post_type' => 'recipe',
    'orderby' => 'date',
    'order' => 'DESC',
    'post_status' => 'publish'
);

$last_two = new WP_Query($args);

if($last_two->have_posts()) : while ($last_two->have_posts()): $last_two->the_post();
?>

<div class="col-sm-6 col-xs-12 last_two_thumb">
    <div class="img-wrapper">
        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'last-two'); ?>" alt="<?php echo get_the_title(get_the_ID()); ?>" title="<?php echo get_the_title(get_the_ID()); ?>">
    </div>
    <a href="<?php echo get_the_permalink(); ?>" title="<?php echo get_the_title(); ?>" >
       <div class="thumb-title">
           <h1><?php echo mb_strimwidth(get_the_title(), 0, 70, '...'); ?></h1>
           <p><?php echo mb_strimwidth(get_the_excerpt(), 0, 90, '...'); ?></p>
       </div>
    </a>
</div>


<?php endwhile; wp_reset_postdata();  else: ?>

<div class="col-lg-12">
    <h3><?php _e('No posts at the moment', 'pstarter'); ?></h3>
</div>

<?php endif; ?>
