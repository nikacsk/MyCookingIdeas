<?php

$args = array(
    'posts_per_page' => 3,
    'post_type' => 'post',
    'orderby' => 'date',
    'order' => 'DESC',
    'post_status' => 'publish',
);

$blog = new WP_Query($args);
$count =0;

if($blog->have_posts()) : while ($blog->have_posts()): $count++; $blog->the_post();

    ?>

    <div class="single-blog-post post-id-<?php echo get_the_ID(); ?>">
        <a href="<?php echo get_the_permalink(); ?>">
            <div class="col-xs-4">
                <div class="home-blog-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'grid-image'); ?>')"></div>
            </div>
            <div class="col-xs-8">
                <p><?php echo get_the_title(); ?></p>
            </div>
        </a>
    </div>


<?php endwhile; wp_reset_postdata();  else: ?>

    <div class="col-lg-12">
        <h3><?php _e('No posts at the moment', 'pstarter'); ?></h3>
    </div>

<?php endif; ?>
