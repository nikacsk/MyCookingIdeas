<?php

$args = [
    'post_type' => 'post',
    'posts_per_page' => 3,
    'orderby' => 'rand',
//    'order' => 'DESC',
    'post_status' => 'publish'
];

$recent = new WP_Query($args);

echo '<ul class="inline-related-recipes">';
while($recent->have_posts()): $recent->the_post();
?>

    <li>
        <a href='<?php echo get_the_permalink(); ?>'>
            <div class='related-img-wrapper blog-post-image-wrapper'>
                <img class='img-responsive' src='<?php echo get_the_post_thumbnail_url(get_the_ID(), 'grid-image'); ?>' alt='<?php echo get_the_title(); ?>'>
            </div>
            <div class='related-title'>
                <?php echo get_the_title(); ?>
            </div>
        </a>
    </li>

<?php
endwhile; wp_reset_postdata();
echo '</ul>';
