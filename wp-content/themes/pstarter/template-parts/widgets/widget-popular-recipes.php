<?php

$args = [
    'post_type' => 'recipe',
    'posts_per_page' => 3,
    'orderby' => 'date',
    'order' => 'DESC',
    'post_status' => 'publish'
];

$recent = new WP_Query($args);

$counter= 0;
echo '<ul class="inline-related-recipes">';
while($recent->have_posts()): $counter++; $recent->the_post();
?>

    <li>
        <a href='<?php echo get_the_permalink(); ?>'>
            <?php if($counter == 1){ echo '<div class="winner-wrapper"><div class="tropgy-top-3"><i class="fas fa-trophy"></i></div>winner</div>';} ?>
            <div class='related-img-wrapper'>
                <img class='img-responsive' src='<?php echo get_the_post_thumbnail_url(get_the_ID(), 'grid-image'); ?>' alt='<?php echo get_the_title(); ?>'>
            </div>
            <div class='related-title'>
                <?php echo get_the_title(); ?>
            </div>
        </a>
    </li>

<?php
endwhile; wp_reset_postdata();
echo '</ul>';
