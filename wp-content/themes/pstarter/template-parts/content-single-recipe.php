<?php $isMobile = new Mobile_Detect();

$terms = get_the_terms(get_the_ID(), 'skill_level');
$skillLevel ='';


if(!empty($terms)){
    foreach ($terms as $term) {
         $skillLevel .= $term->name. ' ';
    }
}
?>

<!-- Featured image or gallery --->
<div class="col-md-7 col-sm-6 col-xs-12">
    <img class="img-responsive  img-100" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" alt="<?php echo get_the_title(); ?>">
</div>
<!-- Featured image or gallery end--->


<!-- Recipe Intro --->
<div class="col-md-5 col-sm-6 col-xs-12">
    <h1 class="single-recipe-title"><?php echo get_the_title(); ?></h1>

<!--    <div class="grid-ratings single-rating">-->
<!--        <i class="fas fa-star"></i>-->
<!--        <i class="fas fa-star"></i>-->
<!--        <i class="fas fa-star"></i>-->
<!--        <i class="fas fa-star"></i>-->
<!--        <i class="far fa-star"></i>-->
<!--        <p class="vote-count">57 ratings </p>-->
<!--    </div>-->

    <?php the_content(); ?>


</div>
<!-- Recipe Intro end--->
<div class="separator"></div>
<div class="clearfix"></div>

<div class="col-recipe-content">

    <div class="share-buttons-wrapper">
        <span  class="share-title"><i class="far fa-heart"></i> Share this:</span>
        <div class="addthis_inline_share_toolbox"></div>
    </div>



    <!-- Ingredients -->
    <section id="ingredients">
        <div class="ingredients-header col-lg-12 no-padding">
            <h2 class="recipe-headings"><?php _e('Ingredients', 'pstarter'); ?></h2>

            <ul>

                <?php if(get_field('_recipe_ready_in')): ?>
                    <li>
                        <i class="fas fa-stopwatch"></i>
                        <span><?php echo get_field('_recipe_ready_in') ?> min</span>
                    </li>
                <?php endif; ?>

                <?php if(get_field('_recipe_servings')): ?>
                    <li>
                        <i class="fas fa-utensils"></i>
                        <span><?php echo get_field('_recipe_servings'); ?> </span>
                    </li>
                <?php endif; ?>

                <?php if($skillLevel): ?>
                    <li>
                        <i class="fas fa-trophy"></i>
                        <span><?php echo $skillLevel; ?></span>
                    </li>
                <?php endif; ?>

            </ul>
        </div>

        <div class="ingredients-content">
            <div class="ingredients-body">
                <?php $counter=0; if(have_rows('_recipe_ingredients')): while (have_rows('_recipe_ingredients')) : $counter++; the_row(); ?>
                    <div class="col-lg-6 no-padding-right">
                        <p><?php echo get_sub_field('_recipe_ingredient'); ?></p>
                    </div>
                    <?php
                    if($counter%2 == 0) {echo '<div class="clearfix"></div>';}

                endwhile; endif; ?>
            </div>

            <div class="in-content-banner">
                <span><?php _e('advertisment', 'pstarster'); ?></span>
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Recipe side -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-8248104886241417"
                     data-ad-slot="5161256449"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>

    </section>

    <!-- Ingredients END-->
    <div class="clearfix"></div>

    <!-- Directions -->
    <section id="directions">
        <div class="ingredients-header col-lg-12 no-padding">
            <h2 class="recipe-headings"><?php _e('Directions', 'pstarter'); ?></h2>
        </div>

        <ul class="preparation-info">
            <li>
                <i class="fas fa-stopwatch"></i>
            </li>
            <?php if(get_field('_recipe_prep_time')): ?>
                <li>
                    Prep:
                    <span><?php echo get_field('_recipe_prep_time') ?> min</span>
                </li>
            <?php endif; ?>

            <?php if(get_field('_recipe_cook_time')): ?>
                <li>
                    Cook:
                    <span><?php echo get_field('_recipe_cook_time'); ?> min</span>
                </li>
            <?php endif; ?>

            <?php if(get_field('_recipe_ready_in')): ?>
                <li>
                    Ready:
                    <span><?php echo get_field('_recipe_ready_in'); ?> min</span>
                </li>
            <?php endif; ?>

        </ul>


        <div class="ingredients-content">
            <div class="ingredients-body">
                <?php $counter=0; if(have_rows('_preparation_steps')): while (have_rows('_preparation_steps')) : $counter++; the_row(); ?>
                    <div class="col-lg-12 no-padding-right">
                        <p class="direction-step-<?php echo $counter; ?> direction-step"><?php echo get_sub_field('_preparation_step'); ?></p>
                    </div>

                    <style>
                        .direction-step-<?php echo $counter; ?>:before {
                            content: '<?php echo $counter; ?>' !important;
                        }
                    </style>
                <?php
                endwhile; endif; ?>
            </div>

            <div class="in-content-banner inline-related-main">
                <?php get_related_recipes(); ?>
            </div>
        </div>
    </section>

    <!-- Directions END-->


    <!-- Image Directions   -->
    <?php if(have_rows('_preparation_steps_images')) : ?>
        <section id="image-directions">
            <div class="image-container">
                <h2 class="recipe-headings"><?php _e('Image directions', 'pstarter'); ?></h2>
                <div class="image-wrapper">
                    <?php while (have_rows('_preparation_steps_images')) : the_row(); ?>
                    <figure>
                        <img src="<?php echo get_sub_field('_preparation_image')['url']; ?>" alt="<?php get_sub_field('_preparation_image')['title']; ?>">
                        <figcaption><?php echo get_sub_field('_preparation_image_description'); ?></figcaption>
                    </figure>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
    <?php endif;?>
    <!-- Image Directions End   -->

    <!-- Video Directions   -->
    <?php if(get_field('_preparation_video')) : ?>
    <section id="video-directions">
        <div class="video-container">
            <h2 class="recipe-headings"><?php _e('Video directions', 'pstarter'); ?></h2>
            <div class="video-wrapper">
                <?php the_field('_preparation_video'); ?>
            </div>
        </div>
    </section>
    <?php endif;?>
    <!-- Video Directions End   -->

    <div class="share-buttons-wrapper" style="float: left; width: 100%; margin-bottom: 25px;">
        <span  class="share-title"><i class="far fa-heart"></i> Share this:</span>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_inline_share_toolbox"></div>
    </div>


    <section id="recipe_comments">
        <?php
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template('/comments.php');
            endif;
        ?>
    </section>
</div>

