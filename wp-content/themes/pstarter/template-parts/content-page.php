<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Piksel_Starter
 */

?>
<div class="separator"></div>
<div class="clearfix"></div>

<div class="col-recipe-content">
    <h1 class="single-post-blog-title"><?php echo get_the_title(); ?></h1>

    <div class="separator"></div>
    <div class="clearfix"></div>

    <div class="main-blog-content">
        <?php the_content(); ?>
    </div>

    <div class="separator"></div>
    <div class="clearfix"></div>

    <div class="separator"></div>
    <div class="clearfix"></div>
</div>
