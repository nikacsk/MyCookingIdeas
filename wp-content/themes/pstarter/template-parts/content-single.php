<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Piksel_Starter
 */

?>
<div class="separator"></div>
<div class="clearfix"></div>

<div class="col-recipe-content">
    <h1 class="single-post-blog-title"><?php echo get_the_title(); ?></h1>

    <div class="share-buttons-wrapper">
        <span  class="share-title"><i class="far fa-heart"></i> Share this:</span>
        <div class="addthis_inline_share_toolbox"></div>
    </div>
    <div class="separator"></div>
    <div class="clearfix"></div>

    <img class="img-responsive  img-100" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" alt="<?php echo get_the_title(); ?>">
    <div class="separator"></div>
    <div class="clearfix"></div>

    <div class="main-blog-content">
        <?php the_content(); ?>
    </div>

    <div class="separator"></div>
    <div class="clearfix"></div>

    <div class="share-buttons-wrapper">
        <span  class="share-title"><i class="far fa-heart"></i> Share this:</span>
        <div class="addthis_inline_share_toolbox"></div>
    </div>

    <div class="separator"></div>
    <div class="clearfix"></div>

    <?php related_from_category(); ?>
</div>
