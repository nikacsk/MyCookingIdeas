<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Piksel_Starter
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function pstarter_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'pstarter_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function pstarter_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'pstarter_pingback_header' );


/**
 * CONSTANT FOR THE TEMPLATE DIRECTORY URL
 */


define( "__THEMEURL__" , get_template_directory_uri());
define( "__THEMEPATH__" , get_template_directory());
define( "__THEMEINC__" , get_template_directory(). '/inc');


/**
 * REMOVE TITLE PREFIX
 */

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

        $title = single_cat_title( '', false );

    } elseif ( is_tag() ) {

        $title = single_tag_title( '', false );

    } elseif ( is_author() ) {

        $title = '<span class="vcard">' . get_the_author() . '</span>' ;

    }

    return $title;

});


/**
 * LOADING NAVIGATION
 */

    function bootstrap_navigation($theme_location, $container_id, $depth = 2, $container = 'div', $container_class = 'collapse navbar-collapse', $menu_class = 'nav navbar-nav'){

                wp_nav_menu( array(
                        'menu'              => $theme_location,
                        'theme_location'    => $theme_location,
                        'depth'             => $depth,
                        'container'         => $container,
                        'container_class'   => $container_class,
                        'container_id'      => $container_id,
                        'menu_class'        => $menu_class,
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker())
                );
    }


/**
 * Thumbnail sizes
 */


add_image_size('last-two', 585, 350, true);
add_image_size('grid-image', 400, 400, true);


add_action('restrict_manage_posts','restrict_listings_by_taxonomy');
function restrict_listings_by_taxonomy() {
    global $typenow;
    global $wp_query;
    if ($typenow=='listing') {
        $taxonomy = 'skill_level';
        $business_taxonomy = get_taxonomy($taxonomy);
        wp_dropdown_categories(array(
            'show_option_all' =>  __("Show All {$business_taxonomy->label}"),
            'taxonomy'        =>  $taxonomy,
            'name'            =>  'business',
            'orderby'         =>  'name',
            'selected'        =>  $wp_query->query['term'],
            'hierarchical'    =>  true,
            'depth'           =>  3,
            'show_count'      =>  true, // Show # listings in parens
            'hide_empty'      =>  true, // Don't show businesses w/o listings
        ));
    }
}


function get_related_recipes() {
    global $post;
    $article_category = wp_get_post_terms($post->ID, 'recipe_type');
    $categorySlug = $article_category[0]->slug;

    $args = [
            'post_type' => 'recipe',
            'posts_per_page' => 5,
            'orderby' => 'rand',
            'post_status' => 'publish',
            'tax_query' => array(
            array (
                'taxonomy' => 'recipe_type',
                'field' => 'slug',
                'terms' => $categorySlug,
            )
        ),
    ];

    $recipes = new WP_Query($args);

    if($recipes->have_posts()) {
        $html = '<div class="inline-related-title">Maybe you\'ll like it!</div><ul class="inline-related-recipes">';
        while($recipes->have_posts()) : $recipes->the_post();
        $title = get_the_title();
        $link = get_the_permalink();
        $image = get_the_post_thumbnail_url(get_the_ID(), 'grid-image');

        $html .= "<li>
                    <a href='{$link}'>
                        <div class='related-img-wrapper'>
                            <img class='img-responsive' src='{$image}' alt='{$title}'>
                        </div>
                        <div class='related-title'>
                            {$title}
                        </div>
                    </a>
                  </li>";
        endwhile; wp_reset_postdata();
        $html .= '</ul>';

        echo $html;
    }

//    var_dump($recipes->get_posts()); die;
}


//remove website field in comments
add_filter('comment_form_default_fields', 'website_remove');
function website_remove($fields)
{
    if(isset($fields['url']))
        unset($fields['url']);
    return $fields;
}

//add img responsive class to uploaded images

function add_image_class($class){
    $class .= ' additional-class';
    return $class;
}
add_filter('get_image_tag_class','add_image_class');

//add relate from the blog

    function related_from_category() {
        global $post;
        $categories =get_the_category( $post->ID);
        $category = $categories[0]->slug;

        $args = [
            'post_type' => 'post',
            'posts_per_page' => 12,
            'orderby' => 'rand',
            'post_status' => 'publish',
            'tax_query' => array(
                array (
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => $category,
                )
            ),
        ];

        $posts = new WP_Query($args);

        echo '<div class="col-lg-12"><div style="margin-bottom: 15px;" class="inline-related-title">More from '.$categories[0]->name.'</div></div>';
        if($posts->have_posts()){
            while($posts->have_posts()){
                $posts->the_post();
                echo '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">';
                echo '<a class="related-articles" href="'.get_the_permalink().'">
                        <div class=\'related-img-wrapper\'>
                            <img class=\'img-responsive\' src="'. get_the_post_thumbnail_url(get_the_ID(), 'grid-image').'" alt="'.get_the_title() .'">
                        </div>
                        <div class=\'related-title\'>'.  get_the_title().'</div>
                    </a>';
                echo '</div>';
            }
        }
    }

function wpbeginner_numeric_posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );

    echo '</ul></div>' . "\n";

}
