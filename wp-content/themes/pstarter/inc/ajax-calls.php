<?php


function grid_load_more(){

    $offset= $_POST['grid_offset'];

    $args = array(
        'posts_per_page' => 8,
        'post_type' => 'recipe',
        'orderby' => 'date',
        'order' => 'DESC',
        'post_status' => 'publish',
        'offset' => $offset
    );


    $grid = new WP_Query($args);
    $count =0;
    if($grid->have_posts()) : while ($grid->have_posts()): $count++; $grid->the_post();

        $terms = get_the_terms(get_the_ID(), 'recipe_type');
        $terms2 = get_the_terms(get_the_ID(), 'skill_level');
        $skillLevel ='';

        foreach ($terms2 as $term) {
            $skillLevel .= $term->name. ' ';
        }
        ?>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
            <div class="grid-post-wrapper post-id-<?php echo get_the_ID(); ?>">
                <a href="<?php echo get_the_permalink(); ?>">
                    <!--                --><?php //the_post_thumbnail('grid-image', array('class'=>'img-responsive')); ?>

                    <div class="grid-image-wrap">
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'grid-image'); ?>" alt="<?php echo get_the_title(get_the_ID()); ?>" title="<?php echo get_the_title(get_the_ID()); ?>">
                    </div>

                    <div class="post-details">
<!--                        <div class="grid-ratings">-->
<!--                            <i class="fas fa-star"></i>-->
<!--                            <i class="fas fa-star"></i>-->
<!--                            <i class="fas fa-star"></i>-->
<!--                            <i class="fas fa-star"></i>-->
<!--                            <i class="far fa-star"></i>-->
<!---->
<!--                            <span class="vote-count">(57)</span>-->
<!--                        </div>-->

                        <div class="grid-title">
                            <?php echo mb_strimwidth(get_the_title(), 0, 75, '...'); ?>
                        </div>

                    </div>

                </a>
                <span class="rec-category">
                <?php foreach ($terms as $term){

                    $termLink = get_term_link($term);
                    echo "<a href='{$termLink}'>{$term->name}</a>";
                } ?>
            </span>

                <ul class="meal-details">
                    <?php if(get_field('_recipe_ready_in', get_the_ID())): ?>
                        <li>
                            <i class="fas fa-stopwatch"></i>
                            <span><?php echo get_field('_recipe_ready_in', get_the_ID()) ?> <?php _e('min'); ?></span>
                        </li>
                    <?php endif; ?>

                    <?php if(get_field('_recipe_servings', get_the_ID())): ?>
                        <li>
                            <i class="fas fa-utensils"></i>
                            <span><?php echo get_field('_recipe_servings', get_the_ID()); ?> <?php _e('people'); ?></span>
                        </li>
                    <?php endif; ?>

                    <?php if($skillLevel): ?>
                        <li>
                            <i class="fas fa-trophy"></i>
                            <span><?php echo $skillLevel; ?></span>
                        </li>
                    <?php endif; ?>


                </ul>

            </div>
        </div>
        <?php

         endwhile; wp_reset_postdata();

         else:
            return null;
         endif;

        exit;
}



add_action('wp_ajax_grid_load_more', 'grid_load_more'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_grid_load_more', 'grid_load_more'); // wp_ajax_nopriv_{action}
