<?php
$classes = array_diff(scandir(__THEMEINC__.'/widgets/Classes'), array('..', '.'));

foreach ($classes as $class){

    require_once __THEMEINC__.'/widgets/Classes/'.$class;
}

// Register and load the widget
function mci_load_widgets() {
    $classes = array_diff(scandir(__THEMEINC__.'/widgets/Classes'), array('..', '.'));

    foreach ($classes as $class){
        $className = str_replace('.php', '', $class);
        register_widget( "{$className}" );
    }
}
add_action( 'widgets_init', 'mci_load_widgets' );





