<?php

function pstarter_scripts() {

    wp_register_style( 'pstarter-style', get_stylesheet_uri() );
    wp_register_style( 'pstarter-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_register_style( 'pstarter-build', get_template_directory_uri() . '/assets/css/build/app.min.css' );

    wp_deregister_script('jquery');
    wp_enqueue_style('pstarter-style');
    wp_enqueue_style('pstarter-bootstrap');
    wp_enqueue_style('pstarter-build');


    //AJAX LOAD MORE SCRIPT

    global $wp_query;


    // register our main script but do not enqueue it yet
    wp_register_script( 'pstarter-build-js', get_stylesheet_directory_uri() . '/assets/js/build/app.min.js', array(), '20151215', true );



    wp_localize_script( 'pstarter-build-js', 'pstarter', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
        'load_button_text' => __('More recipes', 'pstarter'),
        'no_grid_posts' => __('There are no more recipes to be loaded ...', 'pstarter')
    ) );

    wp_enqueue_script( 'pstarter-build-js' );
}
add_action( 'wp_enqueue_scripts', 'pstarter_scripts' );


function add_async_attribute($tag, $handle) {

    if ( 'jquery' !== $handle )
        return $tag;
    return str_replace( ' src', ' integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous" src', $tag );
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
