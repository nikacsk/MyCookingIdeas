<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Piksel_Starter
 */

//var_dump(Myci_Form_Dropdowns::recipeTypes());

//die;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="theme-color" content="#f06724" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<?php wp_head(); ?>
</head>

<body id="starter-<?php echo get_the_ID(); ?>" <?php body_class(); ?>>

<!-- header -->
<header id="top_header">
    <nav id="top_menu">
        <div class="container-fluid no-padding">
            <div class="navbar-header top-navbar-header">
                <a id="main_menu_toggle" href="#" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top_menu_collapse" aria-expanded="false"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></a>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
               <div class="social-media">

                   <p><?php _e('Follow us on: ') ?></p>

                   <a href="https://www.facebook.com/mycookingideasofficial" target="_blank">
                       <i class="fab fa-facebook-square"></i>
                   </a>

                   <a href="https://www.pinterest.com/mycookingideas/" target="_blank">
                       <i class="fab fa-pinterest-square"></i>
                   </a>

                   <a href="https://www.youtube.com/channel/UCq9khpxwZn7OYbSKREaTalQ" target="_blank">
                       <i class="fab fa-youtube-square"></i>
                   </a>
               </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <?php bootstrap_navigation('top', 'top_menu_collapse' , '1', 'div', 'collapse navbar-collapse navbar-right'); ?>
            </div>
        </div>
    </nav>
    <!-- MAIN MENU -->
    <nav id="main_menu" class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header">
                <a id="main_menu_toggle" href="#" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_menu_collapse" aria-expanded="false"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></a>
                <a style="display: block; padding: 5px;" class="navbar-brand" href="<?php bloginfo( 'url' ); ?>"><img src="<?php echo __THEMEURL__ ; ?>/assets/images/logoAR.svg" /></a>
            </div>

            <?php bootstrap_navigation('primary', 'main_menu_collapse' , '4', 'div', 'collapse navbar-collapse navbar-right'); ?>

    <!-- MAIN MENU END -->
</header>
<!-- header end -->

<div id="page-id-<?php echo get_the_ID(); ?>"  class="container no-padding width-100">
