module.exports = function(grunt) {

    grunt.initConfig({

        concat: {
            options: {
                stripBanners: {
                    options: {
                        block: true, // all block comments are removed
                    },
                }
            },
            dist: {
                src: [
                    'assets/css/fontawesome-all.min.css',
                    // 'assets/css/bootstrap.css',
                    'assets/css/style.css',
                    'assets/css/myci-filter.css'
                ],
                dest: 'assets/css/build/app.css',
            },
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'assets/css/build',
                    src: ['*.css', '!*.min.css'],
                    dest: 'assets/css/build/',
                    ext: '.min.css'
                }]
            }
        },

        uglify: {
            options: {
                manage: false,
                mangle: false
            },
            my_target: {
                files : {
                    'assets/js/build/app.min.js': [
                        'assets/js/jquery.min.js',
                        'assets/js/skip-link-focus-fix.js',
                        'assets/js/bootstrap.min.js',
                        'assets/js/custom.js',
                        'assets/js/starterloadmore.js'
                    ]
                }
            }
        }

    });



    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('default');
    grunt.registerTask('build', ["concat", "uglify", "cssmin"]);
};