const dictionary = {
    custom: {
        recipeTitle: {
            min: 'Search term need to have more than 3 characters'
        },
        recipeIngredients : {
            min: 'Search term need to have more than 3 characters.',
            regex: 'Multiple search terms need to separated by comma without spaces.'
        },
        recipeTime : {
            regex: 'Search term need to be in format x hours, x hour, x minute or x minutes.'
        }
    }
};

module.exports = dictionary;