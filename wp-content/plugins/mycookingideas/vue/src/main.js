import Vue from 'vue'
import MyciRecipeFilter from './components/RecipeFilter/MyciRecipeFilter'
import VeeValidate from 'vee-validate'
import dictionary from '../validation-messages/validationMessages'

Vue.use(VeeValidate);
Vue.config.productionTip = false;

VeeValidate.Validator.localize('en', dictionary);

new Vue({
  render: h => h(MyciRecipeFilter),
}).$mount('#recipe_filter');
