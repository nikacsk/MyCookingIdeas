<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://labscreative.com
 * @since             1.0.0
 * @package           Myci
 *
 * @wordpress-plugin
 * Plugin Name:       MyCookingIdeas
 * Plugin URI:        https://labscreative.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Nikola Nikoloski
 * Author URI:        https://labscreative.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       myci
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'MYCI_VERSION', '1.0.0' );
define('MYCI_PATH', dirname(__FILE__));
define('MYCI_URL', plugin_dir_url( __FILE__ ));
define('MYCI_API_NAMESPACE', 'myci/v1');
define('MYCI_API_URL', get_site_url(). '/wp-json/myci/v1/');
define('MYCI_VUE_BUILD', 'production');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-myci-activator.php
 */
function activate_myci() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-myci-activator.php';
	Myci_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-myci-deactivator.php
 */
function deactivate_myci() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-myci-deactivator.php';
	Myci_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_myci' );
register_deactivation_hook( __FILE__, 'deactivate_myci' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-myci.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_myci() {

	$plugin = new Myci();
	$plugin->run();

}
run_myci();
