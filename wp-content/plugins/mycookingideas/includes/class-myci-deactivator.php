<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://labscreative.com
 * @since      1.0.0
 *
 * @package    Myci
 * @subpackage Myci/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Myci
 * @subpackage Myci/includes
 * @author     Nikola Nikoloski <n.nikoloski21@gmail.com>
 */
class Myci_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
