<?php

/**
 * Creates Wordpress REST API routes
 *
 * @link       http://labscreative.com
 * @since      1.0.0
 *
 * @package    Myci
 * @subpackage Myci/includes
 */

class Myci_Api_Routes {
    public static function register_api_routes() {
        //Content Routes
        register_rest_route( MYCI_API_NAMESPACE, '/recipes', array(
            'methods' => 'GET',
            'callback' => ['Myci_Api','getRecipes'],
        ) );

        //HELPER ROUTES
        register_rest_route( MYCI_API_NAMESPACE, '/getRecipeFormData', array(
            'methods' => 'GET',
            'callback' => ['Myci_Api','getRecipeFormData'],
        ) );
    }
}
