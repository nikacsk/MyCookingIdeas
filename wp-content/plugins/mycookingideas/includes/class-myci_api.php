<?php

/**
 * Creates Wordpress REST API callbacks logic
 *
 * @link       http://labscreative.com
 * @since      1.0.0
 *
 * @package    Auas
 * @subpackage Auas/includes
 */

class Myci_Api {

    public static function getRecipes ( WP_REST_Request $request)
    {
        $args = [
            'post_type' => 'recipe',
            'post_status' => 'publish',
            'order' => 'DESC',
            'orderby' => 'date',
            'posts_per_page' => '8'
        ];

        if($request->get_param('offset'))
            $args['offset'] = sanitize_text_field($request->get_param('offset'));

        if($request->get_param('posts_per_page'))
            $args['posts_per_page'] = sanitize_text_field($request->get_param('posts_per_page'));

        if($request->get_param('recipeTitle'))
            $args['s'] = sanitize_text_field($request->get_param('recipeTitle'));

        if($request->get_param('recipeType')) {
            $args['tax_query'][] = [
                    'taxonomy' => 'recipe_type',
                    'field' => 'id',
                    'terms' => sanitize_text_field($request->get_param('recipeType')),
                ];
        }

        if($request->get_param('recipeSkill')) {
            $args['tax_query'][] = [
                    'taxonomy' => 'skill_level',
                    'field' => 'id',
                    'terms' => sanitize_text_field($request->get_param('recipeSkill')),
                ];
        }

        if($request->get_param('recipeDate'))
            $args['order'] = sanitize_text_field($request->get_param('recipeDate'));

        if($request->get_param('recipeTime')) {
            $timeString = sanitize_text_field($request->get_param('recipeTime'));
            $minutes = '';

            if(strpos($timeString, 'hours') !== false || strpos($timeString, 'hour') !== false) {
                $hoursInteger = (int) $timeString;

                $minutes =  $hoursInteger * 60;
            }

            if(strpos($timeString, 'minutes') !== false) {
                $minutes = (int) $timeString;
            }

            $args['meta_query'][] = [
                'key'       => '_recipe_ready_in',
                'compare'   => '<=',
                'value'     =>  intval($minutes),
                'type' => 'numeric'
            ];

            $args['meta_query'][] = [
                'key'       => '_recipe_ready_in',
                'compare'   => 'NOT IN',
                'value'     =>  ['', 'days'],
                'type' => 'numeric'
            ];

        }
        if($request->get_param('recipeIngredients')) {
            $randId = rand(0, 100);
            $args['meta_query'][$randId] = ['relation' => 'OR'];
            if(strpos(sanitize_text_field($request->get_param('recipeIngredients')), ',') !== false) {
                $terms = explode(',' , sanitize_text_field($request->get_param('recipeIngredients')));
                foreach ($terms as $term) {
                    $args['meta_query'][$randId][] = [
                        'key'       => '_ingredients_for_querying',
                        'compare'   => 'LIKE',
                        'value'     =>  $term
                    ];
                }
            } else {
                $args['meta_query'][] = [
                    'key'       => '_ingredients_for_querying',
                    'compare'   => 'LIKE',
                    'value'     =>  sanitize_text_field($request->get_param('recipeIngredients'))
                ];
            }
        }

        $recipes = new WP_Query($args);

        if(!$recipes->have_posts())
            return new WP_REST_Response(['error' => __('There are not any posts to load.')], 400);

        $response = [];

        foreach ($recipes->posts as $key=>$recipe){
            $response[$key]['recipe_id'] = $recipe->ID;
            $response[$key]['recipe_url'] = get_the_permalink($recipe->ID);
            $response[$key]['recipe_title'] = mb_strimwidth($recipe->post_title, 0, 75, '...');
            $response[$key]['recipe_excerpt'] = $recipe->post_excerpt;
            $response[$key]['recipe_image'] = get_the_post_thumbnail_url($recipe->ID, 'grid-image');
            $response[$key]['recipe_type'] = get_the_terms( $recipe->ID, 'recipe_type');
            $response[$key]['recipe_skill'] = get_the_terms( $recipe->ID, 'skill_level');
            $response[$key]['recipe_servings'] = get_field('_recipe_servings', $recipe->ID) ? get_field('_recipe_servings', $recipe->ID) : '';
            $response[$key]['recipe_ready_in'] = get_field('_recipe_ready_in', $recipe->ID) ? get_field('_recipe_ready_in', $recipe->ID) : '';
            $response[$key]['recipe_date'] = $recipe->post_date;
            $response[$key]['post_type'] = $recipe->post_type;
        }

        return new WP_REST_Response($response);
    }

    public static function getRecipeFormData ()
    {
        $args = [
            'taxonomy' => 'recipe_type',
            'orderby' => 'name',
            'order' => 'asc'
        ];

        $types_response = [];

        $types = get_terms($args);

        foreach ($types as $key => $type) {
            $types_response[$key]['name'] = $type->name;
            $types_response[$key]['term_id'] = $type->term_id;
        }

        $args2 = [
            'taxonomy' => 'skill_level',
            'orderby' => 'name',
            'order' => 'asc'
        ];

        $skills_response = [];

        $skills = get_terms($args2);


        foreach ($skills as $key => $skill) {
            $skills_response[$key]['name'] = $skill->name;
            $skills_response[$key]['term_id'] = $skill->term_id;
        }

        return new WP_REST_Response([
            'recipe_types' => $types_response,
            'skill_level' => $skills_response
        ]);
    }
}