<?php

/**
 * Fired during plugin activation
 *
 * @link       https://labscreative.com
 * @since      1.0.0
 *
 * @package    Myci
 * @subpackage Myci/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Myci
 * @subpackage Myci/includes
 * @author     Nikola Nikoloski <n.nikoloski21@gmail.com>
 */
class Myci_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
