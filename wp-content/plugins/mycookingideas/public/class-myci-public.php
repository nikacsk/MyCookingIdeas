<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://labscreative.com
 * @since      1.0.0
 *
 * @package    Myci
 * @subpackage Myci/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Myci
 * @subpackage Myci/public
 * @author     Nikola Nikoloski <n.nikoloski21@gmail.com>
 */
class Myci_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Myci_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Myci_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/myci-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Myci_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Myci_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */



//		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/myci-public.js', array( 'jquery' ), $this->version, false );
        $source = defined('MYCI_VUE_BUILD') && MYCI_VUE_BUILD === 'dev' ?  'http://localhost:8080/app.js' :  MYCI_URL . 'vue/dist/js/app.9a35bfea.js';
		wp_enqueue_script('app-vue', $source, array(), $this->version, true );

		if(MYCI_VUE_BUILD === 'production')
            wp_enqueue_script( 'vendor-vue', MYCI_URL . 'vue/dist/js/chunk-vendors.afa50b58.js', $source, array(), $this->version, true );

	}


	public function  myciInitApiRoutes ()
    {
        Myci_Api_Routes::register_api_routes();
    }

    public function loadJsGlobals ()
    {
        ?>

        <script>
            window.myciFilter = {
                apiUrl: <?php echo json_encode(MYCI_API_URL); ?>,
                baseUrl: <?php echo json_encode(get_site_url()); ?>,
                outerQueryObject: <?php echo json_encode((object)$_GET); ?>,
            }
        </script>

    <?php
    }

}
